---
title: test_img
date: 2023-04-14 17:08:57
tags:
categories: 新手
excerpt: 测试图片
---

spring aop: 代理， 在其最一般的形式中，是一个作为其他东西的接口的类。代理可以连接任何东西，网络连接、内存中的大对象、文件或其他一些昂贵或无法复制的资源
<!-- more -->
![截图](3e6402c86eed51e44048b77af8cde21c.png)

你会在此处看到带有一组方法签名的通用接口bank。ATM和BankBranck类都实现了这个接口。现在，当一个人去ATM取款时，系统实际上调用代理对象的withdraw(0功能，在这种情况下是ATM示例，然后代理调用BankBranch的withdraw()方法来自其中的实例。BankBranch实例具有功能的实际实现，因此称为真实对象。这样，ATM代理就可以在对BankBranch的实际实例进行实际的withdrwa(0调用之前或之后处理一些额外的处理和操作。现在代理对象可能没有对所有接口方法的实际实现，因此在默写情况下，例如chanPersionalInfo(), 它可能只是覆盖该方法，但给出适当的访问限制消息
