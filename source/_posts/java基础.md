---
title: java基础
date: 2023-04-13 20:27:40
tags:
categories: 面试题
---
spring aop: 代理， 在其最一般的形式中，是一个作为其他东西的接口的类。代理可以连接任何东西，网络连接、内存中的大对象、文件或其他一些昂贵或无法复制的资源
<!-- more -->
![截图](/images/5cc882bad92ae21b5227b9da7c0b4ee0.png)

你会在此处看到带有一组方法签名的通用接口bank。ATM和BankBranck类都实现了这个接口。现在，当一个人去ATM取款时，系统实际上调用代理对象的withdraw(0功能，在这种情况下是ATM示例，然后代理调用BankBranch的withdraw()方法来自其中的实例。BankBranch实例具有功能的实际实现，因此称为真实对象。这样，ATM代理就可以在对BankBranch的实际实例进行实际的withdrwa(0调用之前或之后处理一些额外的处理和操作。现在代理对象可能没有对所有接口方法的实际实现，因此在默写情况下，例如chanPersionalInfo(), 它可能只是覆盖该方法，但给出适当的访问限制消息

<br/>

上述示例的对象和序列图

{% asset_img ../images/3e6402c86eed51e44048b77af8cde21c.png This is an example image %}

![截图](/images/3e6402c86eed51e44048b77af8cde21c.png)

所以上面ATM银行的例子可以从代理模式的角度总结为以下几点：

Subject(Bank):代理和真实主题的通用接口。

Real Subject(Bank Branch): 实现接口的具体主体

Proxy(ATM) : 提供与Real Subject (或子集)相同的接口

1.维护对Real Subject的引用

2.由于它没有Real Subject（轻量级）的所有数据，因此它可以比Real Subject更快地做很多事情。然后调用Real Subject（如果需要）

代理模式在以下情况下发挥作用：

1.我们有重量级对象，我们想实现一个更简单的版本

2。我们不需要Heavy Weight对象的全部功能

3。我们想限制对Heavy Weight对象的访问

4。因为在创建Heavy Weight对象的实例时可能存在时间延迟或复杂的机制

现在俗话说，每件好事都有一系列缺点，这里是代理模式通常带来的几个常见问题

1.身份比较：我们无法进行身份比较，因为我们不知道确切的真实主体，只是一个代理人

2.歧义：客户可能不知道它现在访问的真实主体与之前的主体不同

3。安全问题：因为除了调用真正的主体之外，客户端不知道代理还在做什么

<br/>

<br/>

### 列一个面试常用的java基础清单

以下是一些常用的Java基础面试问题清单：

Java基础语法、数据类型、变量和常量
面向对象编程的四个基本概念：封装、继承、多态和抽象类
接口和抽象类的区别
Java集合框架：List、Set、Map、Queue等
Java异常处理机制：try-catch、throw和throws的使用
Java多线程编程：线程的生命周期、线程的创建和启动、线程同步、线程池
Java IO流：字节流和字符流、文件读写、对象序列化等
Java反射机制：Class类、Constructor类、Method类等
Java注解：自定义注解、注解的元注解、注解的作用和使用
Java虚拟机：内存区域、垃圾回收算法、类加载机制等
Java网络编程：Socket编程、TCP和UDP协议、HTTP协议等
Java设计模式：单例模式、工厂模式、代理模式、观察者模式等
Java常用工具类：StringUtils、DateUtils、MathUtils等
Java 8新特性：Lambda表达式、Stream API、Optional类、接口的默认方法和静态方法等
这些问题只是Java基础知识的冰山一角，面试官还可能会深入询问相关细节和应用场景。因此，在准备面试前，建议从多个角度对Java基础知识进行深入学习和练习。

### 多线程生命周期

多线程的生命周期可以分为以下状态：

1. 新建状态（New）：线程对象被创建但还没有开始执行。
2. 就绪状态（Runnable）：线程已经被创建并且等待系统调度执行，也就是进入线程池中等待分配资源。
3. 运行状态（Running）：线程已经开始执行。
4. 阻塞状态（Blocked）：线程因为等待某个锁或者IO操作等而被暂停执行。
5. 等待状态（Waiting）：线程等待某个特定的条件唤醒，如调用Object.wait()方法。
6. 计时等待状态（Timed Waiting）：线程等待某个特定的条件唤醒，但是有超时时间，如调用Thread.sleep()方法或者Object.wait(long timeout)方法。
7. 终止状态（Terminated）：线程执行完成或者因异常而终止执行。终止状态（Terminated）：线程执行完成或者因异常而终止执行。

这些状态是按照线程从创建到执行结束的顺序排列的，线程状态的转移也是按照这个顺序进行的。例如，新建的线程对象是处于新建状态的，等待系统调度执行后进入就绪状态，等到获取CPU资源之后进入运行状态，如果在执行过程中被阻塞，就会进入阻塞状态等待解除阻塞，如果需要等待某个条件唤醒，则进入等待状态等待被唤醒，如果有超时时间则进入计时等待状态等待超时或者被唤醒，最终线程执行完成或者因异常而终止执行，进入终止状态。

### 线程池

线程池是一种管理和复用线程的机制，它通过预先创建一定数量的线程，以及在需要时动态创建或销毁线程，来有效地控制线程的数量和避免过多的线程创建和销毁操作。线程池中的线程可以被多个任务共享，从而减少了线程创建和销毁的开销，提高了应用程序的性能和效率。

线程池通常由以下几个组件构成：

1. 线程池管理器(Thread Pool Manager)：用于创建和管理线程池。
2. 工作线程(Worker Threads)：线程池中的线程，用于执行具体的任务。
3. 任务队列(Task Queue)：用于存储尚未被执行的任务。

线程池的使用可以避免线程频繁创建和销毁的开销，同时也可以限制同时运行的线程数量，避免系统过载和资源耗尽的情况。通常来说，线程池适用于处理较长时间的任务或大量的任务，比如网络请求、文件处理、数据库查询等。线程池在Java中的实现主要有以下几种方式：

1. Executors类：Java提供了Executors类来创建线程池，可以使用静态方法创建不同类型的线程池，比如FixedThreadPool、CachedThreadPool、SingleThreadPool等。
2. ThreadPoolExecutor类：ThreadPoolExecutor是Java中线程池的底层实现，提供了更灵活的配置和管理线程池的方式。
3. ScheduledThreadPoolExecutor类：ScheduledThreadPoolExecutor是ThreadPoolExecutor的扩展，支持定时和周期性任务的执行。

线程池的使用需要根据具体的应用场景和需求进行配置和调优，可以根据任务类型、系统负载、可用资源等因素来决定线程池的大小、队列大小、线程池类型等参数，以达到最优的性能和效率。同时需要注意线程池的正确关闭和资源释放，避免资源泄漏和程序异常。

### 对于io密集型任务，多线程可以做什么优化

对于IO密集型任务，多线程可以通过以下优化来提高程序的性能：

1. 使用非阻塞IO：在IO密集型任务中，线程的大部分时间都是在等待IO操作完成，如果使用阻塞IO，线程会一直被阻塞，无法利用CPU资源，因此可以使用非阻塞IO，使线程在等待IO操作时不会被阻塞，可以继续处理其他任务。
2. 使用线程池：线程的创建和销毁都需要一定的时间和系统资源，如果任务量很大，频繁地创建和销毁线程会造成性能上的开销。使用线程池可以重复利用已经创建的线程，避免频繁地创建和销毁线程。
3. 使用异步IO：异步IO是指应用程序在发起IO请求后，不需要等待IO操作完成就可以继续处理其他任务，当IO操作完成后，系统会通知应用程序。异步IO可以在IO操作等待的过程中处理其他任务，提高CPU的利用率。
4. 使用多路复用技术：多路复用技术是指在一个线程中可以同时处理多个IO操作。通过使用多路复用技术，可以避免为每个IO操作创建一个线程，降低线程数量，减小系统开销。
5. 缓存数据：缓存可以减少对IO操作的依赖，提高数据访问的速度。在IO密集型任务中，如果数据可以缓存在内存中，就可以减少对IO操作的频率，提高程序的性能。
6. 优化IO操作：在IO密集型任务中，优化IO操作的方式包括使用缓存、批量处理IO请求、减少IO操作次数等。这些方式都可以减少IO操作的开销，提高程序的性能。

### 如何创建线程池，常用的创建线程池的方法

<br/>

在Java中，可以通过java.util.concurrent包下的Executor和ExecutorService接口以及ThreadPoolExecutor类来创建线程池。

常用的创建线程池的方法有以下几种：

使用Executors工具类创建线程池：

```java
ExecutorService executor = Executors.newFixedThreadPool(nThreads);
ExecutorService executor = Executors.newSingleThreadExecutor();
ExecutorService executor = Executors.newCachedThreadPool();
ScheduledExecutorService executor = Executors.newScheduledThreadPool(nThreads);
ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

```

这种方法简单方便，可以快速创建线程池，但是无法对线程池进行详细的配置。

使用ThreadPoolExecutor类手动创建线程池：

```
ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
```

<br/>

这种方法可以对线程池进行详细的配置，如设置核心线程数、最大线程数、线程空闲时间、任务队列、线程工厂和拒绝策略等。

使用Spring框架提供的ThreadPoolTaskExecutor类创建线程池：

```
ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
executor.setCorePoolSize(corePoolSize);
executor.setMaxPoolSize(maximumPoolSize);
executor.setKeepAliveSeconds(keepAliveTime);
executor.setQueueCapacity(queueCapacity);
executor.setThreadNamePrefix(threadNamePrefix);
executor.setRejectedExecutionHandler(rejectedExecutionHandler);
executor.initialize();
```

这种方法基于ThreadPoolExecutor类，但是更加易用，可以通过配置文件进行灵活的配置。

以上三种方法都可以用于创建线程池，具体选择哪种方法取决于具体需求和使用场景。

### ThreadPoolExecutor的参数，以及参数详解，列出一个表格

ThreadPoolExecutor是Java中用于创建线程池的核心类，它有很多参数可以配置。下面是ThreadPoolExecutor的常用参数及其详解：

|参数|类型|说明|
|--|--|--|
|corePoolSize|int|线程池核心线程数，即线程池中一直保持的线程数量|
|maximumPoolSize|||
||||
||||

常用的创建线程池的方法包括：

Executors.newFixedThreadPool(int n)：创建固定线程数的线程池，核心线程数和最大线程数都是n，使用LinkedBlockingQueue作为阻塞队列。

Executors.newSingleThreadExecutor()：创建只有一个线程的线程池，使用LinkedBlockingQueue作为阻塞队列。

Executors.newCachedThreadPool()：创建一个可缓存线程的线程池，核心线程数为0，最大线程数为Integer.MAX_VALUE，使用SynchronousQueue作为阻塞队列。

ThreadPoolExecutor构造方法：使用ThreadPoolExecutor的构造方法可以自定义线程池的参数，例如：

```
ThreadPoolExecutor executor = new ThreadPoolExecutor(
    5, 10, 60L, TimeUnit.SECONDS, 
    new ArrayBlockingQueue<Runnable>(100), 
    new ThreadFactory() {
        public Thread newThread(Runnable r) {
            return new Thread(r);
        }
    }, new ThreadPoolExecutor.AbortPolicy());

```

上面的代码创建了一个核心线程数为5，最大线程数为10，任务队列大小为100的线程池，使用ArrayBlockingQueue作为阻塞队列，线程工厂使用默认的ThreadFactory，拒绝策略使用AbortPolicy。

### 如何进行多线程编程

进行多线程编程时，需要考虑以下几点：

1. 线程安全：多线程环境下，对于共享的资源，需要进行合适的同步和互斥处理，避免出现并发问题，如死锁、竞态等。
2. 线程生命周期：需要考虑线程的启动、运行、暂停、终止等生命周期问题，如何控制线程的状态，避免线程过多或线程不足的问题。
3. 任务划分和调度：对于需要进行多线程处理的任务，需要进行合适的划分和调度，避免出现负载不均、线程饥饿等问题。
4. 线程池的使用：对于需要大量使用线程的场景，可以使用线程池来管理线程，避免频繁创建和销毁线程带来的性能问题。
5. 其他：还需要考虑线程间的通信、异常处理、中断处理等问题。

在进行多线程编程时，可以使用 Java 提供的线程相关的 API，如 Thread、Runnable、ThreadGroup、synchronized、wait/notify 等，也可以使用 Java 并发包中提供的工具类和接口，如 Executor、ThreadPoolExecutor、Future、Semaphore、CountDownLatch 等。同时，还需要根据不同的业务需求选择不同的多线程编程模型，如生产者-消费者模型、多生产者-单消费者模型、多生产者-多消费者模型等。

### 如何使用多线程处理cpu密集型任务

对于CPU密集型任务，多线程并不能提高效率，因为CPU资源是有限的。在这种情况下，可以考虑使用多进程并行处理，即使用多个进程来同时执行任务。

在Java中，可以使用ProcessBuilder类来创建新的进程。每个进程可以独立地运行，各自占用CPU资源。使用进程间通信机制，比如管道、共享内存、socket等，来实现进程之间的数据传输和通信。

另外，Java 8中引入了并行流（parallel stream）的概念，可以将CPU密集型任务分解为多个小任务，使用多个线程并行处理。但是，并行流并不一定能提高效率，具体还要根据任务特点和硬件配置来决定。

