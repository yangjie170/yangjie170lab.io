---
title: 测试语法
date: 2023-04-02 10:36:34
tags:
categories: 新手
permalink: null
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<!-- more -->
java 基础

{% blockquote David Levithan, Wide Awake %}
Do not just seek happiness for yourself. Seek happiness for all. Through kindness. Through mercy.
{% endblockquote %}

引用代码块
{% codeblock lang:objc %}
[rectangle setX: 10 y: 10 width: 20 height: 20];
{% endcodeblock %}

{% codeblock Array.map %}
array.map(callback[, thisArg])
{% endcodeblock %}

{% codeblock _.compact http://underscorejs.org/#compact Underscore.js %}
_.compact([0, 1, false, 2, '', 3]);
=> [1, 2, 3]
{% endcodeblock %}



<br/>

|年/周|第一周|第二周|第三周|第四周|
|--|:--:|:--:|:--:|:--:|
|2022.10|-|1.熟悉planB代码 <br/>2.注册送btc需求讨论 <br/>3、注册送btc建表、开发 <br/>4、activity项目创建进入开发 <br/>5、同步yapi接口文档|1、自测注册送btc接口<br/> 2、修复部分bug<br/> 3、按规范修改代码命名方式 <br/>4、联调短信、邮件接口 <br/>5、短信、邮件的中英文模块切换<br/>|1、补充注册送btc定时开关逻辑 <br/>2、进入邀请返佣开发，需求讨论、建表、开发接口 <br/>3、邀请返佣接口开发，并修复btc活动接口bug <br/>4、表结构数据类型对齐|
|2022.11|1、邀请返佣同步数据接口 <br/>2、通过order服务进行数据清洗<br/>3、修复返佣接口bug<br/>4、app、web端接口开发|1、邀请返佣bug修复<br/>2、世界杯活动开发（需求讨论、建表、开发接口）<br/>3、联调世界杯数据接口<br/>4、空投锁仓开发（需求讨论、建表、接口开发）<br/>5、世界杯接口、锁仓接口联调及bug修复|1、修改锁仓bug<br/>2、更换邮件、短信模板<br/>3、区位码校验、后管统计接口开发<br/>4、人工审核短信自动发送<br/>5、处理后管数据显示问题|1、数据统计日线补零，修复bug<br/>2、添加日历表<br/>3、增加人工提现复审流程<br/>4、个人邀请返佣接口开发<br/>5、实时返佣信息开发|
|2022.12|1、实名认证sdk熟悉<br/>2、增加数据统计接口<br/>3、修复邮件统计bug<br/>4、后管数据问题解决<br/>5、实名认证接口对接，获取license|1、实名认证接口联调<br/>2、生产数据sql编写<br/>3、初级实名流程<br/>4、新手空投开发（需求讨论、建表、接口开发）<br/>5、自测接口，pc端接口开发|1、联调新手空投接口<br/>2、新手空投bug修复<br/>3、新手空投数据统计<br/>4、双旦活动开发<br/>5、定时任务开启|1、瓜分奖励接口开发<br/>2、瓜分奖励接口联调<br/>3、生产邮件问题修复<br/>4、双旦接口联调|
|2023.01|1、ieo活动开发（需求讨论、建表、开发接口）<br/>2、后管ieo活动增删改查接口自测<br/>3、ieo表结构修改<br/>4、web端接口开发<br/>5、ieo接口联调，并修复bug|1、ieo活动bug修复<br/>2、更换国际短信运营商<br/>3、国家码接口增加字段，区分ocr和短信验证字段<br/>4、ieo活动bug修复|1、kyc发布<br/>2、ieo活动上线|-|
|2023.02|1、每日运营邮件增加excel附件<br/>2、活动奖励开发（需求讨论、建表、开发）<br/>3、跟进之前bug修复情况<br/>4、活动奖励接口开发，财务流水<br/>5、后管上次excel检查是否合法数据校验|1、活动奖励开发进度更新<br/>2、双端活动开发接口进行联调<br/>3、跟进短信异常<br/>4、活动奖励发送时检查运营账户余额<br/>5、财务流水微服务调用接口|1、活动奖励后管接口excle校验数据接口优化<br/>2、为活动奖励数据增加版本<br/>3、修复冻结、解冻后余额不足的bug<br/>4、优化后管活动奖励的查询效率<br/>5、优化活动奖励、修复bug|1、修复法币、划转开关接口<br/>2、个人中心优化<br/>3、每日邮件增加接口<br/>4、新增个人中心优化接口<br/>5、每日邮件增加净入金、折算U价格|
|2023.03|1、交接文档编写|-|-|-|


1、开发相关网址

|网址|是否vpn访问|账号|密码|网站说明|
|--|--|--|--|--|
|http://3.115.163.168:85/projects/BIF/summary|是|@mike|@mike|jira，bug修复平台|
|https://jenkins.bifinx.com/jenkins/|是|@mike|@mike|jenkins平台，CICD自动部署|
|https://gitlab.bifinx.com|是|@mike|@mike|代码托管平台|
|http://register.server.com:8848/nacos|是|@mike|@mike|nacos服务注册发现中心、配置中心，需配置本地hosts文件<br/>文件末尾添加 3.0.7.194 register.server.com|
|http://www.sw0610.com/sms-cloud/wel/index|否|UEXYZM|v5vBgv|短信平台，国内-首网， 国内短信运营商|
|https://www.mysubmail.com/console/welcome|否|wade@uexchange.com|BiFinance123|短信、邮件平台，国际-赛邮，国际短信运营商，国内外邮件发送|
|https://in.advance.ai/dashboard|否|Bi Finance SG_test|BiFinance123|测试账号，选择新加坡并输入账号密码， 实名认证对接商，ocr对比，活体检测接口|
|https://in.advance.ai/login?country=SINGAPORE|否|Bi Finance SG|BiFnance123|正式使用账号，取用appkey和密钥|
|https://www.moduyun.com/console/sms/sent.html|否|hi9077416789|OvxNb8lQPS|3.6号新接入短信服务商 , 具体细节@wade， 有新版api和旧版api, 现使用旧版api， 方便自定义短信发送内容|

<br/>

<br/>

<br/>

2、web端页面、后管系统网址

|测试地址|生产地址|是否vpn访问|网站说明|
|--|--|--|--|
|test01.uexch.com|www.bifinance.com|否|bifianance首页|
|test02.uexch.com|api.bifinance.com|否|后管地址|
|test03.uexch.com|manage.bifinance.com|是|管理后台|
|test04.uexch.com|pcA.bifinance.com|否|bifianance首页|
|test05.uexch.com|manageA.bifinance.com|是|管理后台|

<br/>

3、需求文档

|网址|说明|
|--|--|
|https://alidocs.dingtalk.com/document/edit?dentryKey=lB2G9nx9C7Z2GlG1|邀请返佣需求文档|
|https://alidocs.dingtalk.com/document/edit?docKey=e1X3lE3J10vRqJbv&dentryKey=22KrWlPGu2l94o4A&type=d|世界杯活动需求文档|
|https://alidocs.dingtalk.com/document/edit?docKey=av9kqD36LYZmnVxe&dentryKey=mmgweL0ehzo4PePJ&type=d|管理后台锁仓需求|
|https://docs.google.com/document/d/1MJ6-y4ZxyKLEcjNLfvxBqMjOhEfZF-s2NdKDusSnux4/edit?usp=sharing|邀请返拥加一个表|
|https://docs.google.com/document/d/1DUZCfDR4NogecIMeEt8PF2p0otFsjJGZB1Rq65Hx78M/edit?usp=sharing|实名认证需求|
|https://docs.google.com/document/d/1lgaPpxMAg3uXx4HBXCsXF3KbwUoqW5mzvZ-yXBU3Md4/edit?usp=sharing|双旦活动|
|https://docs.google.com/document/d/1vTWlZD8V6HSn8pE1nfXgTwb3Av1qBu3m/edit?usp=sharing&ouid=102022478996495270508&rtpof=true&sd=true|ieo活动|
|https://docs.google.com/document/d/1fUagPLRzM7Dg-B5COWCDRdfCvEP7zdSbvCBt_zqHHWY/edit?usp=sharing|PC端优化改版文档(1.0.80）|
|https://docs.google.com/document/d/1_evxpPIa5XwFs_6pX1CqonG-FAb8dOE1PD8dC2gwE20/edit?usp=sharing|app端页面优化|

<br/>

4、主要模块activity

https://gitlab.bifinx.com/bifinance/bf-activity

活动任务

三方库 : mybatis-plus、easyexcel、rocketmq、nacos-discovery、nacos-config、openfegin、hystrix、undertow、mongodb、mysql-connector-java、amazon、poi、hutool

引用模块 : account、user、market、order

![截图](attachment:a5dafcfcb1157d4892a8f56aed74b0b9)

5、活动模块概览

|interface接口|provider实现类|rpc接口|类功能描述|
|--|--|--|--|
|ActivityRewardRemoteService|ActivityRewardRemoteController|-|活动奖励增删改查基本功能，检查uid时使用了easyexcel, 上传文件检查uid是否合规，财务流水功能移动至account模块|
|ActivityTaskRemoteService|ActivityTaskRemoteController|ActivityTaskServiceClient|活动模块定时任务类， 定时任务写在这里方便schedule模块调用，包含每日运营邮件发送|
|AirDropRemoteService|AirDropRemoteController|-|空投奖励基本功能，|
|BtcActivityAdminRemoteService|BtcActivityAdminController|-|世界杯活动基本功能 - 后台管理系统，增删改查|
|BtcActivityRemoteService|BtcActivityController|BtcActivityServiceClient|世界杯活动-用户端基本功能|
|FootBallActivityRemoteService|FootBallActivityController|-|世界杯结算相关功能|
|HomePageRemoteService|HomePageController|-|首页统计相关接口|
|IEOActivityAdminRemoteService|IEOActivityAdminAdminRemoteController|-|ieo活动-后台管理系统增删改查基本功能|
|IEOActivityRemoteService|IEOActivityRemoteServiceController|-|ieo活动-web和app端，用户相关功能|
|IeoUserActivityRemoteService|IeoActivityController|-|ieo活动-当前用户活动信息功能|
|NoviceAirDropRemoteService|NoviceAirDropRemoteController|-|新手空投相关接口，后管发放奖励接口，与account接口有关联|
|NoviceAirDropUserRemoteService|NoviceAirDropUserRemoteController|NoviceAirDropUserServiceClient|新手空投相关接口，领取奖励，邀请值等信息|
|UserRakeBackRemoteService|UserRakeBackRemoteController|-|用户返佣相关接口，用户个人信息|

<br/>

6、每日运营邮件

引用模块：user、account、order、thirddotc

运营邮件数据来源：https://test04.uexch.com/dashboard/ChartStatistic

user：提供平台注册量、平台实名量等数据

account : 提供  入金、提币人数、

order: 现货交易人数、ctc交易人数、币币交易额、法币交易额、币币订单量、法币订单量

thirdotc：一键买币交易人数

<br/>

7、短信发送功能

模块：user 

短信、邮件发送工具类：UexGmailUtil、UexSmsUtil

|国籍/发送方式|短信|邮件|
|--|--|--|
|国内|首网  ： http://www.sw0610.com/sms-cloud/wel/index， 短信发送|赛邮 ： https://www.mysubmail.com/console/welcome ， 邮件发送|
|国际|赛邮 ： https://www.mysubmail.com/console/welcome ，短信发送|赛邮 ： https://www.mysubmail.com/console/welcome ，邮件发送|

实现类：SmsServiceImpl

具体功能：发送邮件、短信

8、account模块，增加部分activity使用的rpc接口,用于统计用户账户的数据

例如： HomePageRemoteService 、 DepositAndWithdrawListVO

9、order模块： 增加部分acticity使用的rpc接口，用于统计订单相关数据

例如： OrderRpcRemoteController 、UserFriendsTradeAmountBO

<br/>

10、生产导出数据脚本

a.账户内数据：

```sql
select uid, 累计交易额,sum(usdt) usdt,sum(peth) peth, sum(eth) eth

from (select c.uid,累计交易额,
case when account.coin_name = "usdt" then sum(account.amount + account.frozen_amount) end 'usdt',
	case when account.coin_name = "peth" then sum(account.amount + account.frozen_amount) end 'peth',
	case when account.coin_name = "eth" then sum(account.amount + account.frozen_amount) end 'eth'
 from (	 SELECT
	b.uid uid,
	sum( detail.amount * detail.deal_price ) 累计交易额
FROM
	coin_order.tb_entrust_detail detail
	left join (select sum(usdt) usdt,uid from (SELECT
	sum(amount * deal_price) usdt,
	uid 
FROM
	coin_order.tb_entrust_detail
	where create_time BETWEEN "2022-12-14 20:00:00" 
	AND "2022-12-21 20:00:00"
	GROUP BY uid
	union all 
	SELECT
	sum(amount * deal_price) usdt,
	target_uid uid
FROM
	coin_order.tb_entrust_detail
	where create_time BETWEEN "2022-12-14 20:00:00" 
	AND "2022-12-21 20:00:00"
		GROUP BY target_uid) a GROUP BY a.uid 
		HAVING usdt > 30) b on b.uid = detail.uid or b.uid = detail.target_uid
where b.uid is not null
		GROUP BY b.uid) c 
			left join coin_account.tb_user_coin_account account on account.uid = c.uid and  account.coin_name in ("usdt","peth","eth")
			GROUP BY c.uid,account.coin_name,累计交易额 
) e GROUP BY uid,累计交易额
```

b.邀请值排名

```sql
select a.*,b.inviter_uid 卖方邀请人 from (SELECT
	detail.*,recd.inviter_uid 买方邀请人
FROM
	coin_order.tb_entrust_detail detail
	LEFT JOIN coin_user.tb_invite_recd recd on detail.uid = recd.invitee_uid 
WHERE
	recd.inviter_uid in (3019284,3111925,3113269,3113647,3049239,3173490,3193929,3004933,3007491,3111939)
	AND deal_time BETWEEN "2022-12-23 17:30:00" 
	AND "2023-01-07 11:00:00") a LEFT JOIN coin_user.tb_invite_recd b on a.target_uid = b.invitee_uid 
	
union all 
select a.*,b.inviter_uid 卖方邀请人 from (SELECT
	detail.*,recd.inviter_uid 卖方邀请人
FROM
	coin_order.tb_entrust_detail detail
	LEFT JOIN coin_user.tb_invite_recd recd on detail.target_uid = recd.invitee_uid 
WHERE
	recd.inviter_uid in (3019284,3111925,3113269,3113647,3049239,3173490,3193929,3004933,3007491,3111939)
	AND deal_time BETWEEN "2022-12-23 17:30:00" 
	AND "2023-01-07 11:00:00") a LEFT JOIN coin_user.tb_invite_recd b on a.uid = b.invitee_uid 
```

c.四个维度数据

```sql
select uid, sum(trade_amount) amount from (-- 买方交易额
SELECT
	uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 1
	GROUP BY uid 
	union all 
	-- 卖方交易额
	SELECT
	target_uid uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 2
	GROUP BY target_uid ) a 
	GROUP BY a.uid 
	HAVING amount BETWEEN 100 and 999;
	
	select uid, sum(trade_amount) amount from (-- 买方交易额
SELECT
	uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 1
	GROUP BY uid 
	union all 
	-- 卖方交易额
	SELECT
	target_uid uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 2
	GROUP BY target_uid ) a 
	GROUP BY a.uid 
	HAVING amount BETWEEN 1000 and 5999;
	
	select uid, sum(trade_amount) amount from (-- 买方交易额
SELECT
	uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 1
	GROUP BY uid 
	union all 
	-- 卖方交易额
	SELECT
	target_uid uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 2
	GROUP BY target_uid ) a 
	GROUP BY a.uid 
	HAVING amount BETWEEN 6000 and 10000;
	
	select uid, sum(trade_amount) amount from (-- 买方交易额
SELECT
	uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 1
	GROUP BY uid 
	union all 
	-- 卖方交易额
	SELECT
	target_uid uid,sum(amount * deal_price) trade_amount
FROM
	tb_entrust_detail 
WHERE
	deal_time BETWEEN "2022-12-25 00:00:00" 
	AND "2023-01-04 23:59:59"
  and direction = 2
	GROUP BY target_uid ) a 
	GROUP BY a.uid 
	HAVING amount > 10000
```

d、双旦活动数据

```sql
-- 双旦活动1
select b.uid,b.aa 活动期间累计交易额,c.create_time 注册时间,e.nationality_name_cn 国家 from (select a.uid,sum(total) aa from (select uid,sum(amount * deal_price) total from coin_order.tb_entrust_detail where create_time BETWEEN "2022-12-23 17:30:00" and "2023-01-06 11:00:00"  GROUP BY uid
union all 
select target_uid uid,sum(amount * deal_price) total from coin_order.tb_entrust_detail where create_time BETWEEN "2022-12-23 17:30:00" and "2023-01-06 11:00:00"  GROUP BY target_uid ) a 
GROUP BY a.uid
HAVING aa > 50) b LEFT JOIN coin_user.tb_user c on b.uid = c.uid
LEFT JOIN coin_user.tb_user_expand d on d.user_id = c.user_id
 LEFT JOIN coin_user.tb_country_code e on e.nationality = d.nationality 
 
 -- 双旦活动-用户币币资产数据
 select account.uid,coin_name,amount,frozen_amount from coin_account.tb_user_coin_account account
 left join (select b.uid,b.aa 活动期间累计交易额,c.create_time 注册时间,e.nationality_name_cn 国家 from (select a.uid,sum(total) aa from (select uid,sum(amount * deal_price) total from coin_order.tb_entrust_detail where create_time BETWEEN "2022-12-23 17:30:00" and "2023-01-06 11:00:00"  GROUP BY uid
union all 
select target_uid uid,sum(amount * deal_price) total from coin_order.tb_entrust_detail where create_time BETWEEN "2022-12-23 17:30:00" and "2023-01-06 11:00:00"  GROUP BY target_uid ) a 
GROUP BY a.uid
HAVING aa > 50) b LEFT JOIN coin_user.tb_user c on b.uid = c.uid
LEFT JOIN coin_user.tb_user_expand d on d.user_id = c.user_id
 LEFT JOIN coin_user.tb_country_code e on e.nationality = d.nationality ) e on e.uid = account.uid    where type = 1 and (amount > 0 or frozen_amount >0 )
and e.uid is not null
ORDER BY uid 

-- 双旦活动2
SELECT
	ranks.uid,
	d.nationality_name_cn 国家,
	a.create_time 注册时间 ,
	invitation_value_total 总邀请值,
	invitees_num_total 邀请人数,
	friends_trade_amount 好友交易额,
	friends_deposits_amount 好友充值额
FROM
	coin_activity.tb_novice_air_drop_rank ranks
	LEFT JOIN coin_user.tb_user a ON ranks.uid = a.uid
	LEFT JOIN coin_user.tb_user_expand c ON c.user_id = a.user_id
	LEFT JOIN coin_user.tb_country_code d ON c.nationality = d.nationality 
WHERE
	ranks.create_time BETWEEN "2022-12-23 17:30:00" 
	AND "2023-01-06 11:00:00"

-- 双旦活动-用户币币资产数据
select account.uid,coin_name,amount,frozen_amount from coin_account.tb_user_coin_account account LEFT JOIN (SELECT
	ranks.uid,
	d.nationality_name_cn 国家,
	d.create_time 注册时间 ,
	invitation_value_total 总邀请值,
	invitees_num_total 邀请人数,
	friends_trade_amount 好友交易额,
	friends_deposits_amount 好友充值额
	
FROM
	coin_activity.tb_novice_air_drop_rank ranks
	LEFT JOIN coin_user.tb_user a ON ranks.uid = a.uid
	LEFT JOIN coin_user.tb_user_expand c ON c.user_id = a.user_id
	LEFT JOIN coin_user.tb_country_code d ON c.nationality = d.nationality 
WHERE
	ranks.create_time BETWEEN "2022-12-23 17:30:00" 
	AND "2023-01-06 11:00:00") e on e.uid = account.uid where type = 1 and (amount > 0 or frozen_amount >0 )
	and e.uid is NOT null
	ORDER BY account.uid
 
```

<br/>

e.交易数据

```sql
select uid,活动标题,国籍,sum(累计交易金额) 累计交易金额,"USDT" 交易币种, count(累计交易金额) 交易次数  from (
SELECT
	a.uid,
	a.activity_title 活动标题,
	nationality_name_cn 国籍,
	#0 + CAST( IFNULL( sum(c.amount), 0 ) AS CHAR ) 累计交易金额,
	c.amount 累计交易金额,
	"USDT" 交易币种
	#count( c.amount ) 交易次数 
FROM
	tb_btc_activity_user a
	LEFT JOIN tb_btc_activity b ON a.activity_id = b.id
	LEFT JOIN ( 
	SELECT
	user_id,case when direction = 1 then amount when direction = 2 then amount * deal_price end amount
FROM
	coin_order.tb_entrust_detail 
	) c ON ( a.user_id = c.user_id
	) 
	
	union all 
	
	SELECT
	a.uid,
	a.activity_title 活动标题,
	nationality_name_cn 国籍,
	#0 + CAST( IFNULL( sum(c.amount), 0 ) AS CHAR ) 累计交易金额,
	c.amount ,
	"USDT" 交易币种
#	count( c.amount ) 交易次数 
FROM
	tb_btc_activity_user a
	LEFT JOIN tb_btc_activity b ON a.activity_id = b.id
	LEFT JOIN ( 
	SELECT
	target_user_id,case when direction = 2 then amount when direction = 1 then amount * deal_price end amount
FROM
	coin_order.tb_entrust_detail ) c ON ( a.user_id = c.target_user_id
	)
) d 
GROUP BY 
	uid,活动标题,国籍,交易币种
	
```

f.平台活跃人数

```sql
select cc 日期,count(DISTINCT(uid)) 当日交易人数 from (select DATE(create_time) cc,uid  from coin_order.tb_entrust_detail where create_time BETWEEN "2022-12-08 00:00:00" and "2022-12-22 23:59:59" and uid != 1666083
union all 
select DATE(create_time) cc,target_uid uid from coin_order.tb_entrust_detail where create_time BETWEEN "2022-12-08 00:00:00" and "2022-12-22 23:59:59" and target_uid != 1666083) a GROUP BY 日期
```

g.固定用户查询peth、usdt数量

```sql
select uid, register_time 注册时间,  register_ip 注册ip, login_ip 登录ip,sum(持有peth数量) 持有peth数量,sum(持有usdt数量) 持有usdt数量,是否交易,是否充值 from (SELECT
  e.uid,
	register_time,
	register_ip,
	login_ip,
	case when e.coin_name = "peth" then IFNULL(e.amount + e.frozen_amount,0) end '持有peth数量',
	case when e.coin_name = "usdt" then IFNULL(e.amount + e.frozen_amount,0) end '持有usdt数量',
CASE
		WHEN b.user_id IS NULL THEN
		'否' ELSE '是' 
	END '是否交易',
CASE
		WHEN c.from_user_id IS NULL THEN
		'否' ELSE '是' 
	END '是否充值' 
FROM
	(
	SELECT
		user_id,
		uid 
	FROM
		coin_user.tb_user 
	WHERE
	uid IN ( "2994546","2994616","2994709","2994723","2994746","2994769","2994783","2994827","2994841","2994869","2970149","2887077","2994913","2887437","2886784","2986657","2995927","2996830","2996921","2996934","2997066","2997155","2997277","2997297","2997287","2997376","2997258","2997464","2997180","2997487","2997451","2997326","2997359","2997544","2997580","2997529","2997558","2997625","2997422","2997651","2997677","2997635","2997690","2997716","2997752","2997779","2997794","2997848","2997739","2997929","2997908","2997969","2997882","2941112","2997998","2998015","2998034","2888187","2998106","2998207","2998125","2998230","2998294","2998325","2998344","2998382","2994513","2998355","2998405","2998443","2998472","2998370","2997870","2955556","2998510","2998525","2998552","2998565","2998591","2998088","2998610","2998685","2998700","2998728","2998747","2998779","2998799","2998824","2998844","2998861","2998955","2998975","2999036","2999059","2999047","2999070","2999101","2999086","2999139","2999170","2999115","2999180","2999213","2999247","2999295","2999314","2999330","2999351","2999368","2999383","2999426","2999449","2999488","2999527","2999745","2999646","2999773","2998881","2998905","2998927","2999001","2999026","2999795","2999784","2999812","2999838","2999858","2999868","2999889","2999951","2999976","3000004","3000030","3000051","3000062","3000074","3000122","3000156","3000181","3000217","2997816","3000237","3000252","2998392","3000344","3000413","2997667","3000846","3000872","3000891","3000917","3000945","3000967","3000995","3001019","3001053","3000773","3000751","3000740","3000727","3000715","3000661","3000649","3000620","3000625","3000605","3000560","3000577","3000520","3000548","3001138","3001163","3001190","3002301")) a # 用户表
	LEFT JOIN coin_user.tb_user_expand d ON a.user_id = d.user_id # 用户注册表
	LEFT JOIN (select user_id from coin_order.tb_entrust_detail GROUP BY user_id) b ON a.user_id = b.user_id # 是否交易
	LEFT JOIN (select from_user_id from coin_account.tb_deposit_record GROUP BY from_user_id) c ON a.user_id = c.from_user_id # 是否充值
	LEFT JOIN (
	SELECT
		user_id,
		coin_name,
		sum( amount ) amount,
		sum( frozen_amount ) frozen_amount,
		uid 
	FROM
		coin_account.tb_user_coin_account 
	WHERE
		type IN ( 1, 2 ) 
		AND coin_name IN ( "PETH", "usdt" ) 
	GROUP BY
		coin_name,
		user_id,
		uid 
	) e ON a.user_id = e.user_id

) f GROUP BY uid, 注册时间,注册ip,登录ip,是否交易,是否充值
```

h.初级实名充币数量

```sql
-- 初级审核
SELECT
	users.uid,codes.nationality_name_cn 国家,
CASE
		WHEN users.user_verified_status = 1 THEN
		"初级实名" 
		when users.user_verified_status = 0 then "初级实名"
		when users.user_verified_status = 2 then "初级实名"
	END 实名状态,
	deposit.coin_name,
	sum(deposit.amount) 充币金额
	
FROM
	coin_user.tb_user users
	LEFT JOIN coin_account.tb_deposit_record deposit ON users.uid = deposit.uid
	LEFT JOIN coin_user.tb_user_expand expand on expand.user_id = users.user_id  
	LEFT JOIN coin_user.tb_country_code codes on codes.nationality_name = expand.nationality_name
WHERE
	user_verified_status in (0,1,2)
	AND deposit.uid IS NOT NULL
	GROUP BY uid,coin_name,国家
```

i.2022年 23号用户交易额

```sql
select user_uid uid,sum(total) 23号到31号交易总额  from (

select uid user_uid,sum(amount * deal_price) total from coin_order.tb_entrust_detail where create_time  BETWEEN "2023-01-23 08:00:00" and "2023-01-31 08:00:00" and uid in ('3307987','3308084','3308131','3308161','3308224','3308259','3308274','3308298','3308131','3308407','3004981','3308448','3308491','3308509','3308531','3308837','3308814','3184308','3259212','3188157','2959510','2895925','2957994')  GROUP BY user_uid,coin_market
union all 
select target_uid user_uid,sum(amount * deal_price) total from coin_order.tb_entrust_detail where create_time BETWEEN "2023-01-23 08:00:00" and "2023-01-31 08:00:00" and uid in ('3307987','3308084','3308131','3308161','3308224','3308259','3308274','3308298','3308131','3308407','3004981','3308448','3308491','3308509','3308531','3308837','3308814','3184308','3259212','3188157','2959510','2895925','2957994') GROUP BY user_uid,coin_market
) a
GROUP BY uid;

```

j.2.21到2.23号新注册用户交易额大于100

```sql

select c.uid 新用户uid,case when b.inviter_uid = 0 then '无' else b.inviter_uid end 新用户上一级邀请人uid,c.amount 新用户累计交易额 from (

select uid, sum(trade_amount) amount from (-- 买方交易额
SELECT
	uid,sum(amount * deal_price) trade_amount
FROM
	coin_order.tb_entrust_detail 
	GROUP BY uid 
	union all 
	-- 卖方交易额
	SELECT
	target_uid uid,sum(amount * deal_price) trade_amount
FROM
	coin_order.tb_entrust_detail 
	GROUP BY target_uid ) a 
	GROUP BY a.uid 
  HAVING amount > 100

)	c LEFT JOIN coin_user.tb_invite_recd b on b.invitee_uid = c.uid

where b.create_time BETWEEN "2023-02-21 11:00:00" AND "2023-02-28 18:00:00" ORDER BY 新用户累计交易额 desc;


```
